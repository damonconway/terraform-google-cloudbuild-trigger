variable "description" {
  description = "Description of the trigger"
  type        = string
  default     = null
}

variable "disabled" {
  description = "Disable the trigger"
  type        = bool
  default     = false
}

variable "filename" {
  description = "Filename in repo that contains the trigger config"
  type        = string
  default     = null
}

variable "filter" {
  description = ""
  type        = string
  default     = null
}

variable "include_build_logs" {
  description = "Build logs will be sent back to Github"
  type        = string
  default     = null
}

variable "ignored_files" {
  description = "List of globs to match files to ignore"
  type        = list(string)
  default     = []
}

variable "included_files" {
  description = "List of globs to match files to include"
  type        = list(string)
  default     = []
}

variable "location" {
  description = "Cloud Build location for the trigger"
  type        = string
  default     = "global"
}

variable "name" {
  description = "Name for the trigger"
  type        = string
  default     = null
}

variable "project" {
  description = "Google project to use for trigger if different from provider"
  type        = string
  default     = null
}

variable "service_account" {
  description = ""
  type        = string
  default     = null
}

variable "substitutions" {
  description = ""
  type        = map(string)
  default     = {}
}

variable "tags" {
  description = ""
  type        = list(string)
  default     = []
}

variable "approval_config" {
  description = ""
  type = list(object({
    approval_required = optional(bool)
  }))
  default = []
}

variable "bitbucket_server_trigger_config" {
  description = ""
  type = list(object({
    repo_slug                        = string
    project_key                      = string
    bitbucket_server_config_resource = string
    pull_request = optional(list(object({
      branch          = optional(string)
      invert_regex    = optional(string)
      comment_control = optional(string)
    })))
    push = optional(list(object({
      branch       = optional(string)
      tag          = optional(string)
      invert_regex = optional(string)
    })))
  }))
  default = []
}

variable "build" {
  description = ""
  type = list(object({
    images        = optional(list(string))
    logs_bucket   = optional(string)
    queue_ttl     = optional(string)
    substitutions = optional(map(string))
    tags          = optional(map(string))
    timeout       = optional(string)
    artifacts = optional(list(object({
      images = optional(list(string))
      objects = optional(list(object({
        location = optional(string)
        paths    = optional(list(string))
        timing = optional(list(object({
          start_time = optional(string)
          end_time   = optional(string)
        })))
      })))
    })))
    available_secrets = optional(list(object({
      secret_manager = list(object({
        version_name = string
        env          = string
      }))
    })))
    options = optional(list(object({
      disk_size_gb            = optional(number)
      dynamic_substitutions   = optional(bool)
      env                     = optional(list(string))
      log_streaming_option    = optional(string)
      logging                 = optional(string)
      machine_type            = optional(string)
      requested_verify_option = optional(string)
      secret_env              = optional(list(string))
      source_provenance_hash  = optional(string)
      substitution_option     = optional(string)
      worker_pool             = optional(string)
      volumes = optional(list(object({
        name = optional(string)
        path = optional(string)
      })))
    })))
    secret = optional(list(object({
      kms_key_name = string
      secret_env   = optional(map(string))
    })))
    source = optional(list(object({
      storage_source = optional(list(object({
        bucket     = string
        object     = string
        generation = optional(string)
      })))
      repo_source = optional(list(object({
        repo_name     = string
        branch_name   = optional(string)
        commit_sha    = optional(string)
        dir           = optional(string)
        generation    = optional(string)
        invert_regex  = optional(string)
        project_id    = optional(string)
        substitutions = optional(map(string))
        tag_name      = optional(string)
      })))
    })))
    step = optional(list(object({
      name             = string
      allow_exit_codes = optional(bool)
      allow_failure    = optional(bool)
      args             = optional(list(string))
      dir              = optional(string)
      entrypoint       = optional(string)
      env              = optional(list(string))
      id               = optional(string)
      script           = optional(string)
      secret_env       = optional(map(string))
      timeout          = optional(string)
      wait_for         = optional(list(string))
      timing = optional(list(object({
        start_time = optional(string)
        end_time   = optional(string)
      })))
      volumes = optional(list(object({
        name = string
        path = string
      })))
    })))
  }))
  default = []
}

variable "git_file_source" {
  description = ""
  type = list(object({
    path                     = string
    repo_type                = string
    github_enterprise_config = optional(string)
    repository               = optional(string)
    revision                 = optional(string)
    uri                      = optional(string)
  }))
  default = []
}

variable "github" {
  description = ""
  type = list(object({
    owner                           = optional(string)
    name                            = optional(string)
    enterprise_config_resource_name = optional(string)
    pull_request = optional(list(object({
      branch          = optional(string)
      invert_regex    = optional(string)
      comment_control = optional(string)
    })))
    push = optional(list(object({
      branch       = optional(string)
      tag          = optional(string)
      invert_regex = optional(string)
    })))
  }))
  default = []
}

variable "pubsub_config" {
  description = ""
  type = list(object({
    topic                 = string
    service_account_email = optional(string)
    state                 = optional(string)
    subscription          = optional(string)
  }))
  default = []
}

variable "repository_event_config" {
  description = ""
  type = list(object({
    repository = optional(string)
    pull_request = optional(list(object({
      branch          = optional(string)
      invert_regex    = optional(string)
      comment_control = optional(string)
    })))
    push = optional(list(object({
      branch       = optional(string)
      tag          = optional(string)
      invert_regex = optional(string)
    })))
  }))
  default = []
}

variable "source_to_build" {
  description = ""
  type = list(object({
    ref                      = string
    repo_type                = string
    github_enterprise_config = optional(string)
    repository               = optional(string)
    uri                      = optional(string)
  }))
  default = []
}

variable "trigger_template" {
  description = ""
  type = list(object({
    branch_name  = optional(string)
    commit_sha   = optional(string)
    dir          = optional(string)
    invert_regex = optional(string)
    project_id   = optional(string)
    repo_name    = optional(string)
    tag_name     = optional(string)
  }))
  default = []
}

variable "webhook_config" {
  description = ""
  type = list(object({
    secret = string
    state  = optional(string)
  }))
  default = []
}
