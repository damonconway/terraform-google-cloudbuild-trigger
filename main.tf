resource "google_cloudbuild_trigger" "this" {
  description        = var.description
  disabled           = var.disabled
  filename           = var.filename
  filter             = var.filter
  include_build_logs = var.include_build_logs
  ignored_files      = var.ignored_files
  included_files     = var.included_files
  location           = var.location
  name               = var.name
  project            = var.project
  service_account    = var.service_account
  tags               = var.tags

  dynamic "git_file_source" {
    for_each = toset(var.git_file_source)
    iterator = gfs

    content {
      path                     = gfs.value.path
      repo_type                = gfs.value.repo_type
      github_enterprise_config = lookup(gfs.value, "github_enterprise_config", null)
      repository               = lookup(gfs.value, "repository", null)
      revision                 = lookup(gfs.value, "revision", null)
      uri                      = lookup(gfs.value, "uri", null)
    }
  }

  dynamic "repository_event_config" {
    for_each = toset(var.repository_event_config)
    iterator = rec

    content {
      repository = lookup(rec.value, "respository", null)

      dynamic "pull_request" {
        for_each = toset(lookup(rec.value, "pull_request", []))
        iterator = recpr

        content {
          branch          = lookup(recpr.value, "branch", null)
          comment_control = lookup(recpr.value, "comment_control", null)
          invert_regex    = lookup(recpr.value, "invert_regex", false)
        }
      }

      dynamic "push" {
        for_each = toset(lookup(rec.value, "push", []))
        iterator = recp

        content {
          branch       = lookup(recp.value, "branch", null)
          invert_regex = lookup(recp.value, "invert_regex", false)
          tag          = lookup(recp.value, "tag", null)
        }
      }
    }
  }

  dynamic "source_to_build" {
    for_each = toset(var.source_to_build)
    iterator = stb

    content {
      ref                      = stb.value.ref
      repo_type                = stb.value.repo_type
      github_enterprise_config = lookup(stb.value, "github_enterprise_config", null)
      repository               = lookup(stb.value, "repository", null)
      uri                      = lookup(stb.value, "uri", null)
    }
  }

  dynamic "trigger_template" {
    for_each = toset(var.trigger_template)
    iterator = tt

    content {
      branch_name  = lookup(tt.value, "branch_name", null)
      commit_sha   = lookup(tt.value, "commit_sha", null)
      dir          = lookup(tt.value, "dir", null)
      invert_regex = lookup(tt.value, "invert_regex", false)
      project_id   = lookup(tt.value, "project_id", null)
      repo_name    = lookup(tt.value, "repo_name", null)
      tag_name     = lookup(tt.value, "tag_name", null)
    }
  }

  dynamic "github" {
    for_each = toset(var.github)
    iterator = gh

    content {
      enterprise_config_resource_name = lookup(gh.value, "enterprise_config_resource_name", null)
      name                            = lookup(gh.value, "name", null)
      owner                           = lookup(gh.value, "owner", null)

      dynamic "pull_request" {
        for_each = toset(lookup(gh.value, "pull_request", []))
        iterator = ghpr

        content {
          branch          = lookup(ghpr.value, "branch", null)
          comment_control = lookup(ghpr.value, "comment_control", null)
          invert_regex    = lookup(ghpr.value, "invert_regex", false)
        }
      }

      dynamic "push" {
        for_each = toset(lookup(gh.value, "push", []))
        iterator = ghp

        content {
          branch       = lookup(ghp.value, "branch", null)
          invert_regex = lookup(ghp.value, "invert_regex", false)
          tag          = lookup(ghp.value, "tag", null)
        }
      }
    }
  }

  dynamic "bitbucket_server_trigger_config" {
    for_each = toset(var.bitbucket_server_trigger_config)
    iterator = bstc

    content {
      bitbucket_server_config_resource = bstc.value.bitbucket_server_config_resource
      project_key                      = bstc.value.project_key
      repo_slug                        = bstc.value.repo_slug

      dynamic "pull_request" {
        for_each = toset(lookup(bstc.value, "pull_request", []))
        iterator = bstcpr

        content {
          branch          = bstcpr.value.branch
          comment_control = lookup(bstcpr.value, "comment_control", null)
          invert_regex    = lookup(bstcpr.value, "invert_regex", false)
        }
      }

      dynamic "push" {
        for_each = toset(lookup(bstc.value, "push", []))
        iterator = bstcp

        content {
          branch       = lookup(bstcp.value, "branch", null)
          invert_regex = lookup(bstcp.value, "invert_regex", false)
          tag          = lookup(bstcp.value, "tag", null)
        }
      }
    }
  }

  dynamic "pubsub_config" {
    for_each = toset(var.pubsub_config)
    iterator = psc

    content {
      topic                 = psc.value.topic
      service_account_email = lookup(psc.value, "service_account_email", null)
      subscription          = lookup(psc.value, "subscription", null)
      state                 = lookup(psc.value, "state", null)
    }
  }

  dynamic "webhook_config" {
    for_each = toset(var.webhook_config)
    iterator = wc

    content {
      secret = psc.value.secret
      state  = lookup(psc.value, "state", null)
    }
  }

  dynamic "approval_config" {
    for_each = toset(var.approval_config)
    iterator = ac

    content {
      approval_required = lookup(ac.value, "approval_required", null)
    }
  }

  dynamic "build" {
    for_each = toset(var.build)
    iterator = b

    content {
      images        = lookup(b.value, "images", null)
      logs_bucket   = lookup(b.value, "logs_bucket", null)
      queue_ttl     = lookup(b.value, "queue_ttl", null)
      substitutions = lookup(b.value, "substitutions", {})
      tags          = lookup(b.value, "tags", [])
      timeout       = lookup(b.value, "timeout", null)

      dynamic "source" {
        for_each = toset(lookup(b.value, "source", []))
        iterator = bsrc

        content {
          dynamic "storage_source" {
            for_each = toset(lookup(bsrc.value, "storage_source", []))
            iterator = bssrc

            content {
              bucket     = bssrc.value.bucket
              object     = bssrc.value.object
              generation = lookup(bssrc.value, "generation", null)
            }
          }

          dynamic "repo_source" {
            for_each = toset(lookup(bsrc.value, "repo_source", []))
            iterator = bsrs

            content {
              repo_name     = bsrs.value.repo_name
              branch_name   = lookup(bsrs.value, "branch_name", null)
              commit_sha    = lookup(bsrs.value, "commit_sha", null)
              dir           = lookup(bsrs.value, "dir", null)
              invert_regex  = lookup(bsrs.value, "invert_regex", null)
              project_id    = lookup(bsrs.value, "project_id", null)
              substitutions = lookup(bsrs.value, "substitutions", null)
              tag_name      = lookup(bsrs.value, "tag_name", null)
            }
          }
        }
      }

      dynamic "secret" {
        for_each = toset(lookup(b.value, "secret", []))
        iterator = bs

        content {
          kms_key_name = bs.value.kms_key_name
          secret_env   = lookup(bs.value, "secret_env", null)
        }
      }

      dynamic "available_secrets" {
        for_each = toset(lookup(b.value, "available_secrets", []))
        iterator = bas

        content {
          dynamic "secret_manager" {
            for_each = toset(bas.value.secret_manager)
            iterator = bassm

            content {
              env          = bassm.value.env
              version_name = bassm.value.version_name
            }
          }
        }
      }

      dynamic "step" {
        for_each = toset(lookup(b.value, "step", []))
        iterator = bstp

        content {
          name             = bstp.value.name
          allow_exit_codes = lookup(bstp.value, "allow_exit_codes", [])
          allow_failure    = lookup(bstp.value, "allow_failure", false)
          args             = lookup(bstp.value, "args", [])
          dir              = lookup(bstp.value, "dir", null)
          entrypoint       = lookup(bstp.value, "entrypoint", null)
          env              = lookup(bstp.value, "env", [])
          id               = lookup(bstp.value, "id", null)
          script           = lookup(bstp.value, "script", [])
          secret_env       = lookup(bstp.value, "secret_env", [])
          timeout          = lookup(bstp.value, "timeout", null)
          timing           = lookup(bstp.value, "timing", null)
          wait_for         = lookup(bstp.value, "wait_for", null)

          dynamic "volumes" {
            for_each = toset(lookup(bstp.value, "volumes", []))
            iterator = bstpv

            content {
              name = bstpv.value.name
              path = bstpv.value.path
            }
          }
        }
      }

      dynamic "artifacts" {
        for_each = toset(lookup(b.value, "artifacts", []))
        iterator = ba

        content {
          images = lookup(ba.value, "images", [])

          dynamic "objects" {
            for_each = toset(lookup(ba.value, "objects", []))
            iterator = bao

            content {
              location = lookup(bao.value, "location", null)
              paths    = lookup(bao.value, "paths", [])

              dynamic "timing" {
                for_each = toset(lookup(bao.value, "timing", []))
                iterator = baot

                content {
                  start_time = lookup(baot.value, "start_time", null)
                  end_time   = lookup(baot.value, "end_time", null)
                }
              }
            }
          }
        }
      }

      dynamic "options" {
        for_each = toset(lookup(b.value, "options", []))
        iterator = bo

        content {
          env                     = lookup(bo.value, "env", [])
          disk_size_gb            = lookup(bo.value, "disk_size_gb", null)
          dynamic_substitutions   = lookup(bo.value, "dynamic_substitutions", false)
          log_streaming_option    = lookup(bo.value, "log_streaming_option", null)
          logging                 = lookup(bo.value, "logging", null)
          machine_type            = lookup(bo.value, "machine_type", null)
          requested_verify_option = lookup(bo.value, "requested_verify_option", null)
          secret_env              = lookup(bo.value, "secret_env", [])
          source_provenance_hash  = lookup(bo.value, "source_provenance_hash", [])
          substitution_option     = lookup(bo.value, "substitution_option", null)
          worker_pool             = lookup(bo.value, "worker_pool", null)

          dynamic "volumes" {
            for_each = toset(lookup(bo.value, "volumes", []))
            iterator = bov

            content {
              name = bov.value.name
              path = bov.value.path
            }
          }
        }
      }
    }
  }
}
